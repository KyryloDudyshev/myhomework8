package myhomework8.main.java.com.danit.hw8;

import java.util.HashSet;
import java.util.List;

public interface FamilyDao {
    public List getAllFamilies();
    public Family getFamilyByIndex(int index);
    public boolean deleteFamily(int index);
    public boolean deleteFamily(Family family);
    public void saveFamily(Family family);


}
