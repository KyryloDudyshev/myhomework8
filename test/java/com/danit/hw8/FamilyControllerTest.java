package myhomework8.test.java.com.danit.hw8;

import myhomework8.main.java.com.danit.hw8.*;

import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

class FamilyControllerTest {
    Man john = new Man("John","Wick",1980);
    HashMap<DayOfWeek,String> scheduleJohn = new HashMap<DayOfWeek,String>();
    Woman karla = new Woman("Karla","Bruni",1980);
    Family familyWick = new Family(karla, john);
    Dog dog = new Dog("Alfa",3,95, new HashSet<String>(Set.of("play","go for a walk")));
    Woman natali = new Woman("Natali","Portman",1990);

    Woman liza = new Woman("Liza","Mineli",2008);
    Man arnold = new Man("Arnold","Schwarzenegger",1960);
    Woman monica = new Woman("Monica","Belucci",1962);
    Family familyArnold = new Family(monica, arnold);
    DomesticCat cat = new DomesticCat("Olaf",4,50, new HashSet<String>(Set.of("sleep","eat")));
    Woman michel = new Woman("Michel","Schwarzeneger",1992);

    HashSet<Pet> wikcsPets = new HashSet<>();
    HashSet<Pet> arnoldPets = new HashSet<>();

    List<Family> familyList = new ArrayList<>();

    RoboCat roboCat = new RoboCat("Martin",2,100,new HashSet<String>(Set.of("sleep","eat")));
    FamilyController familyController = new FamilyController(new FamilyService(new CollectionFamilyDao(familyList)));

    @Test
    void getAllFamilies() {
        familyList.add(familyArnold);
        familyList.add(familyWick);
        assertEquals(familyList,familyController.getAllFamilies());
    }

    @Test
    void getFamiliesBiggerThan() {
        List<Human> children = new ArrayList<>();
        children.add(michel);
        children.add(monica);
        familyWick.setChildren(new ArrayList<>(children));
        familyList.add(familyArnold);
        familyList.add(familyWick);
        List<Family> wick = new ArrayList<>();
        wick.add(familyWick);
        assertEquals(wick,familyController.getFamiliesBiggerThan(2));
    }

    @Test
    void getFamiliesLessThan() {
        List<Human> children = new ArrayList<>();
        children.add(michel);
        children.add(monica);
        familyWick.setChildren(new ArrayList<>(children));
        familyList.add(familyArnold);
        familyList.add(familyWick);
        List<Family> arnold = new ArrayList<>();
        arnold.add(familyArnold);
        assertEquals(arnold,familyController.getFamiliesLessThan(3));
    }

    @Test
    void countFamiliesWithMemberNumber() {
        List<Human> children = new ArrayList<>();
        children.add(michel);
        children.add(monica);
        familyWick.setChildren(new ArrayList<>(children));
        familyList.add(familyArnold);
        familyList.add(familyWick);
        List<Family> arnold = new ArrayList<>();
        arnold.add(familyArnold);
        assertEquals(1,familyController.countFamiliesWithMemberNumber(2));
    }

    @Test
    void createNewFamily() {
        familyList.add(familyArnold);
        familyList.add(familyWick);
        familyController.createNewFamily(monica,arnold);
        assertEquals(3,familyList.size());
    }

    @Test
    void bornChild() {
        familyController.bornChild(familyWick,"Shon","Susanna");
        assertEquals(1,familyWick.getChildren().size());
    }

    @Test
    void adoptChild() {
        familyController.adoptChild(familyWick,monica);
        assertEquals(1,familyWick.getChildren().size());
    }

    @Test
    void deleteAllChildrenOlderThen() {
        familyList.add(familyWick);
        familyWick.addChild(new Man("Shon","Wick",2015));
        familyWick.addChild(new Man("Susan","Wick",2008));
        familyController.deleteAllChildrenOlderThen(2010);
        assertEquals(1,familyWick.getChildren().size());
    }

    @Test
    void count() {
        familyList.add(familyArnold);
        familyList.add(familyWick);
        assertEquals(2,familyController.count());

    }

    @Test
    void getPets() {
        familyList.add(familyWick);
        HashSet<Pet> wikcsPets = new HashSet<>();
        Dog dog = new Dog("Alfa",3,95, new HashSet<String>(Set.of("play","go for a walk")));
        wikcsPets.add(dog);
        HashSet<Pet> expected = new HashSet<>();
        expected.add(dog);
        familyWick.setPetsSet(wikcsPets);
        assertEquals(expected,familyController.getPets(0));

    }

    @Test
    void addPet() {
        familyList.add(familyWick);
        HashSet<Pet> wikcsPets = new HashSet<>();
        familyWick.setPetsSet(wikcsPets);
        Dog dog = new Dog("Alfa",3,95, new HashSet<String>(Set.of("play","go for a walk")));
        familyController.addPet(0,dog);
        assertEquals(1,wikcsPets.size());
    }
}