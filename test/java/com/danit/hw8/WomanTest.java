package myhomework8.test.java.com.danit.hw8;
import myhomework8.main.java.com.danit.hw8.DayOfWeek;
import myhomework8.main.java.com.danit.hw8.Family;
import myhomework8.main.java.com.danit.hw8.Man;
import myhomework8.main.java.com.danit.hw8.Woman;
import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;

class WomanTest {
    Woman woman = new Woman("Alexa","Paterson",1980,300,new HashMap<DayOfWeek,String>());
    Man man = new Man("Joshua","Slow",1980,198,new HashMap<DayOfWeek,String>());
    Family slow = new Family(woman,man);




    @Test
    void bornChildCorrectSurname() {
        woman.setFamily(slow);
        man.setFamily(slow);
        assertEquals("Slow",woman.bornChild().getSurname());
    }

    @Test
    void bornChildCorrectIq() {
        woman.setFamily(slow);
        man.setFamily(slow);
        assertEquals(249,woman.bornChild().getIq());
    }
}